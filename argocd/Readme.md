# Installation

Argocd CRDs needs to be installed before CRs can be applied

```shell
kubectl apply -k argocd/argocd/ \
  && kubectl wait \
  --for=condition=ready \
  --timeout=300s \
  -l app.kubernetes.io/name=argocd-server \
  -n argocd \
  pod \
  && kubectl apply -k argocd/biletado/
```
