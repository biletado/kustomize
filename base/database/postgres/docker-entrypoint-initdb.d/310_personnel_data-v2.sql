-- TODO: migrate to script and get user/pass from env
\c personnel_v2;

INSERT INTO employees ("name")
VALUES ('Veki Purigisto'),
       ('Mirinda Purigistino'),
       ('Venka Servistino');

CREATE
    EXTENSION IF NOT EXISTS "postgres_fdw";
CREATE SERVER reservations_v2
    FOREIGN DATA WRAPPER postgres_fdw
    OPTIONS (dbname 'reservations_v2');
CREATE USER MAPPING FOR postgres
    SERVER reservations_v2
    OPTIONS (user 'postgres', password 'postgres');
CREATE FOREIGN TABLE reservations
    (
        id uuid default uuid_generate_v4() not null,
        "from" date not null,
        "to" date not null,
        room_id uuid not null
        )
    SERVER reservations_v2
    OPTIONS (schema_name 'public', table_name 'reservations');

-- reservation from 2022-03-10 has no service assigned.
INSERT INTO assignments (employee_id, reservation_id, role)
VALUES ((SELECT id FROM employees WHERE "name" = 'Veki Purigisto'), (SELECT id FROM reservations WHERE "from" = '2022-03-10'), 'cleanup'),
       ((SELECT id FROM employees WHERE "name" = 'Veki Purigisto'), (SELECT id FROM reservations WHERE "from" = '2022-03-01'), 'cleanup'),
       ((SELECT id FROM employees WHERE "name" = 'Mirinda Purigistino'), (SELECT id FROM reservations WHERE "from" = '2022-04-01'), 'cleanup'),
       ((SELECT id FROM employees WHERE "name" = 'Venka Servistino'), (SELECT id FROM reservations WHERE "from" = '2022-03-01'), 'service'),
       ((SELECT id FROM employees WHERE "name" = 'Venka Servistino'), (SELECT id FROM reservations WHERE "from" = '2022-04-01'), 'service');
