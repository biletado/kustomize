CREATE
    DATABASE assets_v3;
\c assets_v3;
CREATE
    EXTENSION IF NOT EXISTS "uuid-ossp";

create table if not exists buildings
(
    id         uuid default uuid_generate_v4() not null
        primary key,
    "name"     text                            not null,
    country_code char(2),
    streetname text,
    housenumber text,
    city text,
    postalcode text,
    deleted_at timestamptz
);

alter table buildings
    owner to postgres;

create table if not exists storeys
(
    id          uuid default uuid_generate_v4() not null
        primary key,
    "name"      text                            not null,
    building_id uuid                            not null
        references buildings,
    deleted_at  timestamptz
);

alter table storeys
    owner to postgres;

create table if not exists rooms
(
    id         uuid default uuid_generate_v4() not null
        primary key,
    "name"     text                            not null,
    storey_id  uuid                            not null
        references storeys,
    deleted_at timestamptz
);

alter table rooms
    owner to postgres;
