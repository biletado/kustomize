\c assets_v2;
DO
$$
    DECLARE
        buildingid buildings.id%TYPE;
        storeyid   storeys.id%TYPE;
    BEGIN
        INSERT INTO buildings (name, address)
        VALUES ('oriente', 'ekzemplo vojo 1')
        RETURNING id INTO buildingid;
        INSERT INTO storeys (name, building_id)
        VALUES ('1. supre', buildingid)
        RETURNING id INTO storeyid;
        INSERT INTO rooms (name, storey_id)
        VALUES ('101', storeyid),
               ('102', storeyid),
               ('103', storeyid);
        INSERT INTO storeys (name, building_id)
        VALUES ('2. supre', buildingid)
        RETURNING id INTO storeyid;
        INSERT INTO rooms (name, storey_id)
        VALUES ('200', storeyid),
               ('210', storeyid);
        INSERT INTO storeys (name, building_id)
        VALUES ('3. supre', buildingid)
        RETURNING id INTO storeyid;


        INSERT INTO buildings (name, address)
        VALUES ('sude', 'ŝablono strato 42')
        RETURNING id INTO buildingid;
        INSERT INTO storeys (name, building_id)
        VALUES ('teretaĝo', buildingid)
        RETURNING id INTO storeyid;
        INSERT INTO rooms (name, storey_id, deleted_at)
        VALUES ('001', storeyid, null),
               ('002', storeyid, null),
               ('003', storeyid, null),
               ('004', storeyid, '2023-11-20T14:27Z');
        INSERT INTO storeys (name, building_id)
        VALUES ('1. supre', buildingid)
        RETURNING id INTO storeyid;
        INSERT INTO rooms (name, storey_id)
        VALUES ('101', storeyid),
               ('102', storeyid),
               ('103', storeyid),
               ('111', storeyid),
               ('112', storeyid);
        INSERT INTO storeys (name, building_id)
        VALUES ('2. supre', buildingid)
        RETURNING id INTO storeyid;
        INSERT INTO rooms (name, storey_id)
        VALUES ('200', storeyid);

        INSERT INTO buildings (name, address, deleted_at)
        VALUES ('Malnova konstruaĵo', '', '2010-12-22T17:30:22Z')
        RETURNING id INTO buildingid;
        INSERT INTO storeys (name, building_id, deleted_at)
        VALUES ('1. supre', buildingid, '2010-12-22T17:30:22Z')
        RETURNING id INTO storeyid;
        INSERT INTO rooms (name, storey_id, deleted_at)
        VALUES ('101', storeyid, '2010-12-22T17:30:22Z'),
               ('102', storeyid, '2010-12-22T17:30:22Z'),
               ('103', storeyid, '2010-12-22T17:30:22Z');
    END
$$ LANGUAGE plpgsql;
