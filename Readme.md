# Installation

```shell
# create the biletado namespace
kubectl create namespace biletado
# optional: set the curren default namespace to "biletado" for easier work with kubectl without "-n"
kubectl config set-context --current --namespace biletado
# apply the application into the new namespace and prune outdated objects
kubectl apply -k overlays/kind --prune -l app.kubernetes.io/part-of=biletado -n biletado # ignore deprecation warning
# wait for all pods to be available
kubectl rollout status deployment -n biletado -l app.kubernetes.io/part-of=biletado --timeout=120s
kubectl wait pods -n biletado -l app.kubernetes.io/part-of=biletado --for condition=Ready --timeout=120s
# now open your browser on http://localhost:9090
```

```shell
kubectl apply -k overlays/kind --prune -l app.kubernetes.io/part-of=biletado -n biletado --prune-allowlist=/v1/configmap --prune-allowlist=/v1/service --prune-allowlist=apps/v1/deployment # TODO
```
See also [prune-whitelist defaults](https://github.com/kubernetes/kubernetes/blob/e39a0af5ce0a836b30bd3cce237778fb4557f0cb/staging/src/k8s.io/kubectl/pkg/util/prune/prune.go#L28-L50)

## with apply-set
```shell
KUBECTL_APPLYSET=true kubectl apply -k overlays/kind --prune --applyset=applyset-biletado -n biletado
```
how to delete it? TODO: file a bug
```shell
kubectl delete -k overlays/kind -n biletado
kubectl delete secret applyset-biletado
```

# Extend this and run on kind
Create your own `kustomization.yaml` and start with the following content:
```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
  - https://gitlab.com/biletado/kustomize.git//overlays/kind?ref=main
```

Deploy your kustomized biletado
```shell
kubectl create namespace biletado
kubectl apply -k /directory/with/your/kustomization --prune -l app.kubernetes.io/part-of=biletado -n biletado
kubectl wait pods -n biletado -l app.kubernetes.io/part-of=biletado --for condition=Ready --timeout=120s
```

## patches
Do more kustomizations with patches

### changing the image of the assets-backend

```yaml
patches:
  - patch: |-
      - op: replace
        path: /spec/template/spec/containers/0/image
        value: registry.gitlab.com/biletado/backend/assets/laravel:latest
    target:
      group: apps
      version: v1
      kind: Deployment
      name: assets
```

#### local images
Using locally created images needs some preparation

```shell
buildah bud -t example.com/alocalimage:version .
kind load docker-image example.com/alocalimage:version -n kind-cluster
# kind load needs docker compatiblity (https://github.com/kubernetes-sigs/kind/issues/2038)
# workaround:
podman save example.com/alocalimage:version --format oci-archive -o alocalimage.tar
kind load image-archive alocalimage.tar -n kind-cluster
```

> The image needs to contain the full path to a repository, otherwise `kind load` tries to prepend `docker.io/library`

The `imagePullPolicy` must also be patched in this case:

```yaml
patches:
  - patch: |-
      - op: replace
        path: /spec/template/spec/containers/0/imagePullPolicy
        value: Never
```
##### full example
```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
  - https://gitlab.com/biletado/kustomize.git//overlays/kind

patches:
  - target:
      kind: Deployment
      name: assets
    patch: |-
      - op: replace
        path: /spec/template/spec/containers/0/image
        value: example.com/alocalimage:version
      - op: replace
        path: /spec/template/spec/containers/0/ports/0/containerPort
        value: 9001
      - op: replace
        path: /spec/template/spec/containers/0/imagePullPolicy
        value: Never
```
